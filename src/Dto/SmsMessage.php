<?php

namespace NotificationChannels\SmsRu\Dto;

class SmsMessage
{
    /**
     * @var string
     */
    private $to;

    /**
     * @var null|string
     */
    private $from = null;

    /**
     * @var null|string
     */
    private $text = null;

    /**
     * @param string $to
     * @param string|null $text
     */
    public function __construct(string $to, ?string $text)
    {
        $this->to = $to;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return $this->from;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $from
     *
     * @return $this
     */
    public function setFrom(?string $from): self
    {
        $this->from = $from;

        return $this;
    }
}