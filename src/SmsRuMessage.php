<?php

namespace NotificationChannels\SmsRu;

class SmsRuMessage
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $sender;

    /**
     * @var string
     */
    public $number;

    /**
     * @param null|string $text
     */
    public function __construct(string $text = null)
    {
        $this->setText($text);
    }

    /**
     * @param string $content
     *
     * @return static
     */
    public static function create($text = null)
    {
        return new static($text);
    }

    /**
     * @param $text
     *
     * @return $this
     */
    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param $sender
     *
     * @return $this
     */
    public function setFrom($sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * @param string $number
     *
     * @return $this
     */
    public function setTo(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return bool
     */
    public function toNotGiven(): bool
    {
        return !$this->number;
    }

    /**
     * @return bool
     */
    public function senderNotGiven(): bool
    {
        return !$this->sender;
    }
}
