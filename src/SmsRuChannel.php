<?php

namespace NotificationChannels\SmsRu;

use NotificationChannels\SmsRu\Drivers\DriverInterface;
use NotificationChannels\SmsRu\Dto\SmsMessage;
use NotificationChannels\SmsRu\Exceptions\CouldNotSendNotification;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Event;

class SmsRuChannel
{
    /**
     * @var DriverInterface
     */
    protected $driver;

    /**
     * @var string
     */
    protected $sender;

    /**
     * @param DriverInterface $driver
     * @param string $sender
     */
    public function __construct(DriverInterface $driver, $sender = null)
    {
        $this->driver = $driver;
        $this->sender = $sender;
    }

    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     *
     * @throws \NotificationChannels\SmsRu\Exceptions\CouldNotSendNotification
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toSmsRu($notifiable);

        if (is_string($message)) {
            $message = SmsRuMessage::create($message);
        }

        if ($message->toNotGiven()) {
            if (!$to = $notifiable->routeNotificationFor('sms_ru')) {
                throw CouldNotSendNotification::missingRecipient();
            }
            $message->setTo($to);
        }

        if ($message->senderNotGiven()) {
            $message->setFrom($this->sender);
        }

        $sms = new SmsMessage($message->number, $message->text);
        $sms->setFrom($message->sender);
        $response = $this->driver->sendSms($sms);

        Event::fire(new SmsWasSended($response, $notifiable));
    }
}
