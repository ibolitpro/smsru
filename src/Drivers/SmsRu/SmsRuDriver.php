<?php

namespace NotificationChannels\SmsRu\Drivers\SmsRu;

use NotificationChannels\SmsRu\Drivers\DriverInterface;
use NotificationChannels\SmsRu\Dto\SmsMessage;

class SmsRuDriver implements DriverInterface
{
    private const CODE_DESCRIPTION = [
        '100' => 'Сообщение принято к отправке. На следующих строчках вы найдете идентификаторы отправленных сообщений в том же порядке, в котором вы указали номера, на которых совершалась отправка.',
        '200' => 'Неправильный api_id.',
        '201' => 'Не хватает средств на лицевом счету.',
        '202' => 'Неправильно указан получатель.',
        '203' => 'Нет текста сообщения.',
        '204' => 'Имя отправителя не согласовано с администрацией.',
        '205' => 'Сообщение слишком длинное (превышает 8 СМС).',
        '206' => 'Будет превышен или уже превышен дневной лимит на отправку сообщений.',
        '207' => 'На этот номер (или один из номеров) нельзя отправлять сообщения, либо указано более 100 номеров в списке получателей.',
        '208' => 'Параметр time указан неправильно.',
        '209' => 'Вы добавили этот номер (или один из номеров) в стоп-лист.',
        '210' => 'Используется GET, где необходимо использовать POST.',
        '211' => 'Метод не найден.',
        '212' => 'Текст сообщения необходимо передать в кодировке UTF-8 (вы передали в другой кодировке).',
        '220' => 'Сервис временно недоступен, попробуйте чуть позже.',
        '230' => 'Сообщение не принято к отправке, так как на один номер в день нельзя отправлять более 60 сообщений.',
        '300' => 'Неправильный token (возможно истек срок действия, либо ваш IP изменился).',
        '301' => 'Неправильный пароль, либо пользователь не найден.',
        '302' => 'Пользователь авторизован, но аккаунт не подтвержден (пользователь не ввел код, присланный в регистрационной смс).',
    ];

    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @param \GuzzleHttp\Client $transport
     * @param string $apiId
     */
    public function __construct(\GuzzleHttp\Client $transport, string $apiId)
    {
        $this->client = new Client\Client($transport, $apiId);
    }

    /**
     * @param SmsMessage $smsMessage
     *
     * @return array
     */
    public function sendSms(SmsMessage $smsMessage): array
    {
        $params = [
            'to'   => $smsMessage->getTo(),
            'text' => $smsMessage->getText(),
            'from' => $smsMessage->getFrom(),
        ];

        $response = $this->client->request('sms/send', $params);

        $ids = [];

        if (isset($response['sms'])) {
            foreach ($response['sms'] as $numb => $smsResponse) {
                if (isset($smsResponse['sms_id'])) {
                    $ids[] = $smsResponse['sms_id'];
                }
            }
        }

        return [
            'code'        => $response['status_code'],
            'description' => $this->getCodeDescription($response['status_code']),
            'sms'         => isset($response['sms']) ? $response['sms'] : [],
            'balance'     => $response['balance'] ?? null,
            'ids'         => $ids,
        ];
    }

    /**
     * @param string $code
     *
     * @return string
     */
    private function getCodeDescription(string $code): string
    {
        return isset(self::CODE_DESCRIPTION[$code]) ? self::CODE_DESCRIPTION[$code] : 'Неизвестный тип ответа sms.ru';
    }
}