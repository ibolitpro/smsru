<?php

namespace NotificationChannels\SmsRu\Drivers\SmsRu\Client;

interface ClientInterface
{
    /**
     * @param string $method
     * @param array $params
     *
     * @return array
     */
    public function request(string $method, array $params = []): array;
}