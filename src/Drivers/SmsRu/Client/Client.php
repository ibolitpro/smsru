<?php

namespace NotificationChannels\SmsRu\Drivers\SmsRu\Client;

class Client implements ClientInterface
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $transport;

    /**
     * @var string
     */
    private $apiId;

    /**
     * @param \GuzzleHttp\Client $transport
     * @param string $apiId
     */
    public function __construct(\GuzzleHttp\Client $transport, string $apiId)
    {
        $this->transport = $transport;
        $this->apiId = $apiId;
    }

    /**
     * @param string $method
     * @param array $params
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request(string $method, $params = []): array
    {
        $options = [
            'form_params'  => $params,
            'query' => [
                'api_id' => $this->apiId,
                'json'   => 1,
            ]
        ];

        $response = $this->transport->request('post', $method, $options);

        if ($response->getStatusCode() !== 200) {
            throw new \Exception("Sms.Ru unavailable");
        }

        return json_decode((string)$response->getBody(), true);
    }
}