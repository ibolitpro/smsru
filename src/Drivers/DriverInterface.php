<?php

namespace NotificationChannels\SmsRu\Drivers;

use NotificationChannels\SmsRu\Dto\SmsMessage;

interface DriverInterface
{
    /**
     * @param SmsMessage $smsMessage
     *
     * @return array
     */
    public function sendSms(SmsMessage $smsMessage): array;
}