<?php

namespace NotificationChannels\SmsRu;

class SmsWasSended
{
    /**
     * @var array
     */
    public $metadata = [];

    /**
     * @var object
     */
    public $notifiable;

    /**
     * @param array $metadata
     * @param $notifiable
     */
    public function __construct(array $metadata, $notifiable)
    {
        $this->metadata = $metadata;
        $this->notifiable = $notifiable;
    }
}
